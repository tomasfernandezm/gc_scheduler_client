require 'spec_helper'

RSpec.describe 'Green Scheduler Client' do
  let(:client_scheduler) { GcSchedulerClient::GreenSchedulerClient.new(end_point: 'anEndpoint', token: 'aToken') }

  describe 'fire_and_forget' do

    it 'should create fire and forget job' do
      job_info = {
        retry_on_failure: true,
        verb: 'GET',
        url: 'http://www.google.com.ar/?FireAndForget'
      }

      job_id = client_scheduler.fire_and_forget(job_info)

      expect { job_id.to_i }.to_not raise_exception Exception
      expect(job_id.to_i > 0).to be true
    end

    it 'should create fire and forget job without retries' do
      job_info = {
        retry_on_failure: false,
        verb: 'GET',
        url: 'http://FireAndForgetWithoutRetriesJob/?FireAndForgetWithoutRetries'
      }

      job_id = client_scheduler.fire_and_forget(job_info)

      expect { job_id.to_i }.to_not raise_exception Exception
      expect(job_id.to_i > 0).to be true
    end
  end

  describe 'delayed' do
    it 'should create delayed job' do
      job_info = {
        retry_on_failure: false,
        verb: 'GET',
        url: 'http://www.google.com.ar/?Delayed'
      }

      job_id = client_scheduler.delayed(job_info, 0, 0, 0, 20)

      expect { job_id.to_i }.to_not raise_exception Exception
      expect(job_id.to_i > 0).to be true
    end
  end

  describe 'recurring' do
    it 'should create recurring job' do
      job_info = {
        verb: 'GET',
        url: 'http://www.google.com.ar/?Delayed'
      }

      response = client_scheduler.recurring('pepe', job_info, '*/1 * * * *')

      expect(response).to eq Rails.application.secrets.scheduler_integration['scheduler_app_name'] + '.pepe'
    end
  end

  describe 'continuation' do
    it 'should create continuation job' do
      jobs_infos = [
        { verb: 'GET', url: 'http://www.google.com.ar/?Cont01' },
        { verb: 'GET', url: 'http://www.google.com.ar/?Cont02' },
      ]

      job_id = client_scheduler.continuation(jobs_infos)

      expect { job_id.to_i }.to_not raise_exception Exception
      expect(job_id.to_i > 0).to be true
    end
  end

  describe 'batch' do
    it 'should create batch deleting preexistent' do
      client_scheduler.recurring_add_to_batch('pepe__01', { verb: 'GET', url: 'http://www.google.com.ar/?Recurring-batch-01' }, '*/2 * * * *')
      client_scheduler.recurring_add_to_batch('pepe__02', { verb: 'GET', url: 'http://www.google.com.ar/?Recurring-batch-02' }, '*/3 * * * *')
      client_scheduler.recurring_add_to_batch('pepe__03', { verb: 'GET', url: 'http://www.google.com.ar/?Recurring-batch-03' }, '*/4 * * * *')

      response = client_scheduler.commit_batch

      expect(response['jobs'].length).to be 3
      expect(response['continuations'].length).to be 0
    end
  end

end
